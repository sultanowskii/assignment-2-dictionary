%include "lib.inc"
%include "colon.inc"

global find_word


; find entry in dict that matches s
;
; args:
; rdi: s
; rsi: dict
;
; locals:
; rsp+0:  node
; rsp+8:  orig_s
; 
; returns:
; entry_addr or NULL
%define node   0
%define orig_s 8
find_word:
    sub rsp, 16
    mov [rsp+orig_s], rdi
    mov [rsp+node], rsi

    .loop:
        mov rax, [rsp+node]
        test rax, rax
        jz .failure

        mov rdi, rax
        get_colon_key rdi

        mov rsi, rax
        mov rdi, [rsp+orig_s]

        call string_equals
        cmp al, 1
        je .success

        mov rdi, [rsp+node]
        get_colon_next rdi
        mov [rsp+node], rax

        jmp .loop

    .success:
    mov rax, [rsp+node]
    jmp .end

    .failure:
    xor eax, eax

    .end:
    add rsp, 16
    ret
