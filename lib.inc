extern string_length
extern string_copy
extern string_equals

extern exit

extern print_string
extern print_newline
extern print_char
extern print_uint
extern print_int

extern read_char
extern read_word

extern parse_uint
extern parse_int
