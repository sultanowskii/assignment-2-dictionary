BINARY_NAME=test.elf

.PHONY: all clean test

all: build

%.o: %.asm
	nasm -felf64 -o $@ $<

main.o: main.asm lib.inc dict.inc colon.inc

dict.o: dict.asm lib.inc

build: $(BINARY_NAME)

$(BINARY_NAME): main.o lib.o dict.o
	ld -o $@ $^

clean:
	$(RM) *.o $(BINARY_NAME)

test: build
	python test.py
