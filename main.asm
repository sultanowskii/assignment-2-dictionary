%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define STDIN  0
%define STDERR 2

%define SYS_READ  0
%define SYS_WRITE 1

%define MAX_INPUT_SIZE    255
%define INPUT_BUFFER_SIZE MAX_INPUT_SIZE + 1

global _start

section .bss

    input_buffer: resb INPUT_BUFFER_SIZE


section .rodata:

    not_found_msg: db 'Entry not found.', 10, 0


section .text

; read input to `input_buffer`
read_input:
    mov rdi, STDIN
    mov rsi, input_buffer
    mov rdx, MAX_INPUT_SIZE
    mov rax, SYS_READ
    syscall

    lea rdi, [rsi + rax - 1]
    cmp byte[rdi], `\n`
    jne .end

    .trim:
    mov byte[rdi], 0

    .end:
    ret

; print error message
;
; args:
; rdi: msg
;
; rsp+0: msg
print_error:
    sub rsp, 8
    mov [rsp+0], rdi

    call string_length
    mov rdx, rax

    mov rsi, [rsp+0]
    mov rdi, STDERR
    mov rax, SYS_WRITE
    syscall

    add rsp, 8
    ret

_start:
    call read_input

    mov rdi, input_buffer
    mov rsi, DICT
    call find_word

    test rax, rax
    jz .not_found

    .found:
    mov rdi, rax
    get_colon_value rdi

    mov rdi, rax
    call print_string

    xor rdi, rdi
    jmp exit

    .not_found:
    mov rdi, not_found_msg
    call print_error
    
    mov rdi, -1
    jmp exit
