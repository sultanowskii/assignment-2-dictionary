import subprocess

TARGET_BIN = './test.elf'


def run(cmd: str, stdin: str = '') -> tuple[str, str, int]:
    """
    Run command in shell.

    Returns STDOUT, STDERR, return code.
    """
    process = subprocess.Popen(
        cmd,
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdin += '\n'
    stdout, stderr = process.communicate(input=stdin.encode('utf-8'))
    return_code = process.returncode

    return stdout.decode('utf-8'), stderr.decode('utf-8'), return_code


def test_getting_existing_key():
    stdout, stderr, code = run(f'{TARGET_BIN}', 'tanki online')
    assert stdout == 'le classic'
    assert stderr == ''
    assert code == 0


def test_getting_empty_key():
    stdout, stderr, code = run(f'{TARGET_BIN}', '')
    assert stdout == ''
    assert stderr == 'Entry not found.\n'
    assert code == 0xFF


def test_getting_non_existent_key():
    stdout, stderr, code = run(f'{TARGET_BIN}', 'apple')
    assert stdout == ''
    assert stderr == 'Entry not found.\n'
    assert code == 0xFF


def test_key_length_limit():
    stdout, stderr, code = run(f'{TARGET_BIN}', 'A' * 1000)
    assert stdout == ''
    assert stderr == 'Entry not found.\n'
    assert code == 0xFF


def main():
    tests = [
        test_getting_existing_key,
        test_getting_empty_key,
        test_getting_non_existent_key,
        test_key_length_limit,
    ]

    print(f'Collected {len(tests)} tests')

    for test in tests:
        function_name = test.__name__

        print(f'{function_name}: running...')

        try:
            test()
        except AssertionError:
            print(f'{function_name}: failed')
            raise

        print(f'{function_name}: success')

    print('Tests passed.')


if __name__ == '__main__':
    main()
