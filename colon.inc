%define first_entry_ptr 0

%define COLON_VALUE_PTR_OFFSET 0
%define COLON_NEXT_PTR_OFFSET  8
%define COLON_KEY_OFFSET       16


; colon struct:
; +0:  value_ptr
; +8:  next_ptr
; +16: key
; +??: value
%macro colon 2
    %ifnstr %1
        %error "colon: Key must be a string."
    %endif
    %ifnid %2
        %error "colon: Label must be an identifier."
    %endif

    %2:
        dq %%colon_value
        dq first_entry_ptr

        %%colon_key: db %1, 0
        ; value is specified after macro
        %%colon_value:

    %define first_entry_ptr %2
    %define DICT %2
%endmacro


; get colon key
;
; args:
; %1: register containing colon address
; 
; result:
; rax = colon key address
%macro get_colon_key 1
    lea rax, [ %1 + COLON_KEY_OFFSET ]
%endmacro


; get colon value
;
; args:
; %1: register containing colon address
; 
; result:
; rax = colon value address
%macro get_colon_value 1
    mov rax, [ %1 + COLON_VALUE_PTR_OFFSET ]
%endmacro


; get next colon ptr
;
; args:
; %1: register containing colon address
; 
; result:
; rax = next colon address
%macro get_colon_next 1
    mov rax, [ %1 + COLON_NEXT_PTR_OFFSET ]
%endmacro
